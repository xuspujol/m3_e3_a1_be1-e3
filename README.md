# M3_E3_A1_Be1-E3


# Index
- ### [L'Exercici 1](#exercici-1)
- ### [L'Exercici 2](#exercici-2)
- ### [L'Exercici 3](#exercici-3)
- ### [L'Exercici 4](#exercici-4)
- ### [L'Exercici 5](#exercici-5)
- ### [L'Exercici 6](#exercici-6)
- ### [L'Exercici 7](#exercici-7)
- ### [L'Exercici 8](#exercici-8)
- ### [L'Exercici 9](#exercici-9)
- ### [L'Exercici 10](#exercici-10)
- ### [L'Exercici 11](#exercici-11)
- ### [L'Exercici 12](#exercici-12)
- ### [L'Exercici 13](#exercici-13)

<br><br>


## **Exercici 1:**
**Missatge benvinguda Escriu un missatge de benvinguda per pantalla.** 
<br>
int main()
{
    printf("Benvinguts al sistema del gat ninja\n");
    return 0;
}
![DADESSIMPLES](/Imatges/Exercici1.png)


<br><br>
[Tornar a l'index...](#index)

## **Exercici 2:** 
**Tipus simples.Escriu un enter, un real, i un caràcter per pantalla.**<br><br>

int main()

{
     int enter, enter2;
    double realDouble;
    char caracter;

    enter=99;
    realDouble=3.14;
    caracter='G';
    printf("enter: %i, double: %lf, caracter: %c",enter+2,realDouble,caracter);


![DADESSIMPLES](/Imatges/Selecci%C3%B3_023.png)



[Tornar a l'index...](#index)
<br><br>

## **Exercici 3:** 
**Suma enters.Demana dos enters per teclat i mostra la suma per pantalla.** <br><br>


_   int enter, enter2;
   
    printf("Intro 1r enter a sumar: ");
    scanf("%i" ,&enter);
    printf("Intro 2r enter a sumar: ");
    scanf("%i" ,&enter2);
    printf("El resultat %i+%i = %i",enter, enter2, enter+enter2);

 ![DADESSIMPLES](/Imatges/Selecci%C3%B3_024.png)   

[Tornar a l'index...](#index)
<br><br><br>

## **Exercici 4:** 
**Tipus.Indica el tipus de dades que correspon en cada cas:**

1. Edat: Enter (int)
1. Temperatura: Real (doble)
1. La resposta a la pregunta: Ets solter (s/n)? (if)
1. El resultat d’avaluar la següent expressió: (5>6 o 4<=8):
1. El teu nom. Caracter (char)
<br><br>



<br>
[Tornar a l'index...](#index)
<br><br>


## **Exercici 5:** 
**Números.Mostrar per pantalla els 5 primers nombres naturals.**

<br><br>

Printf("1,2,3,4,5");

![DADESSIMPLES](/Imatges/Selecci%C3%B3_025.png)

<br><br>
[Tornar a l'index...](#index)
<br>

## **Exercici 6:** 
**Suma.Mostrar per pantalla la suma i el producte dels 5 primers nombres naturals.**

printf("La suma: %i, el producte: %i",1+2+3+4+5,1*2*3*4*5);



![DADESSIMPLES](/Imatges/Exercici6.png)



<br><br>
[Tornar a l'index...](#index)
<br>

## **Exercici 7:** 
**Intercanvi.Demana dos enters i intercanvia els valors de les variables.**

int a,b,temp;


printf("Indica el valor de a: ");
    scanf("%i",&a);
printf("Indica el valor de b: ");
    scanf("%i",&b);

printf("El valor de a es: %i, i el valor de b es %i" ,a,b);


return 0;
}

![DADESSIMPLES](/Imatges/Exercici71.png)

<br><br>


int a,b,temp;


printf("Indica el valor de a: ");
    scanf("%i",&a);
printf("Indica el valor de b: ");
    scanf("%i",&b);


    temp=a;
    a=b;
    b=temp;

printf("El valor de a es: %i, i el valor de b es %i" ,a,b);


![DADESSIMPLES](/Imatges/Exercici72.png)






<br><br>
[Tornar a l'index...](#index)
<br>

## **Exercici 8:** 
**Pantalla de menú.Mostrar per pantalla el següent menú:** 

Opcions: <br>
1-Alta <br>
2-Baixa <br>
3-Modificacions <br>
4-Sortir <br>
Tria opció (1-4): 

        printf("Opcions:\n ");
        printf("\n\t1-Alta\n");
        printf("\t2-Baixa\n");
        printf("\t3-Modificacions\n");
        printf("\t4-Sortir\n");
        printf("\nTria opció (1-4)");


![DADESSIMPLES](/Imatges/Exercici8.png)




<br> <br>

[Tornar a l'index...](#index)

## **Exercici 9:** 
**Pantalla de menúMostra per pantalla el següent menú:** 

printf("***************************\n");
  printf("***** \tMENU\ *****\n");
  printf("***************************\n");
    printf("**** \t1-alta\ ****\n");
    printf("**** \t2-baixa\ ***\n");
    printf("**** \t2-sortir\ ****\n");
  printf("***************************\n");


![DADESSIMPLES](/Imatges/Exercici9.png)



<br> <br>



[Tornar a l'index...](#index)

## **Exercici 10:** 
**Càlcul del capital final en interès simple.Feu un programa que resolgui el següent enunciat: Calcular el Capital Final CF d’una inversió, a interès simples, si sabem el Capital Inicial (C0), el nombre d’anys (n), i el rèdit (r). La fórmula per obtenir el CF a interès simples és: Donat que el temps està expressat en anys la k=1. El rèdit ‘r’ ve donat en %. Nota: les dades de l’exercici cal demanar-les per teclat.** 


int entrada[3];
int c0, r, n;
float final;


  printf("Introdueix el valor que te Capital inicial: ");
        scanf("%i",&entrada[0]);
  printf("Introdueix el valor que te redit: ");
        scanf("%i",&entrada[1]);
  printf("Introdueix el valor que te n(nombre d'anys): ");
        scanf("%i",&entrada[2]);

if(c0==entrada[0]);
if(r==entrada[1]);
if(n==entrada[2]);

printf("La teva combinacó de numeros es: %i, %i, %i, \n",entrada[0],entrada[1],entrada[2]);

    final=entrada[0]+((entrada[0]*entrada[1]*entrada[2])/100*1);

printf("El resultat de el Capital Final es:%f ",final);





![DADESSIMPLES](/Imatges/Exercici10.png)






<br> <br>

[Tornar a l'index...](#index)

## **Exercici 11:** 
**Càlcul del Capital final en interès compost.Feu un programa que resolgui el següent enunciat: Calcular el Capital Final CF d’una inversió, a interès compost, si sabem el Capital Inicial (C0), el nombre d’anys (n), i el interès (i). La fórmula per obtenir el CF a interès compost és: L’ interès ‘i’ ve donat en tant per 1.** 

int entrada[3];
int c0, r, n;
float final;


  printf("Introdueix el valor que te Capital inicial: ");
        scanf("%i",&entrada[0]);
  printf("Introdueix el valor que te redit: ");
        scanf("%i",&entrada[1]);
  printf("Introdueix el valor que te n(nombre d'anys): ");
        scanf("%i",&entrada[2]);

if(c0==entrada[0]);
if(r==entrada[1]);
if(n==entrada[2]);

printf("La teva combinacó de numeros es: %i, %i, %i, \n",entrada[0],entrada[1],entrada[2]);

    final=entrada[0]*pow(1+entrada[1],entrada[2]);

printf("El resultat de el Capital Final es:%f ",final);



    return 0;
}

![DADESSIMPLES](/Imatges/Exercici11.png)

<br> <br>

[Tornar a l'index...](#index)

## **Exercici 12:** 
**La travessa.Feu un programa que permeti generar una travessa de forma aleatòria. Tot seguit ha de permetre introduir-ne una per teclat i comprovar-ne els encerts respecte la travessa generada aleatòriament. El format de sortida queda a la vostra elecció sempre que n’informi dels encerts.** 





<br> <br>

[Tornar a l'index...](#index)

## **Exercici 13:** 
**Joc dels daus.Feu un programa que permeti simular el resultat de tirar un dau.Tot seguit ha de demanar un número per teclat, entre 1 i 6, i ens ha dir si és el mateix que el que hem generat aleatòriament.** 


int total;
double aleatori[6];
int entrada[6];


        srand(getpid());
total=0;

aleatori[0]= rand()%6;


    printf("Introdueix un numero del 1 al 6: ");
    scanf("%i",&entrada[0]);


    if(aleatori[0]==entrada[0]) total++;


    printf("Has endivinat %i numeros \n",total);
    printf("El numero del dau es: %.0lf, \n", aleatori[0]);
    printf("El teu numero es: %i \n",entrada[0]);



![DADESSIMPLES](/Imatges/Exercici13.png)

<br> <br>

[Tornar a l'index...](#index)
s